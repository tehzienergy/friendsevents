if ($('#map').length) {

  ymaps.ready(init);

  function init() {

    var duration = 30000;

    var map = new ymaps.Map("map", {
      center: [55.7730288, 37.5699146],
      zoom: 16,
      controls: []
    }, {suppressMapOpenBlock: true} );
    
    
  
    $(window).on('load resize', function() {
      map.destroy();
      ymaps.ready(init);
    });

    function animatedMap() {

      var coord1 = map.getCenter()[0];
      var coord2 = map.getCenter()[1];

      coord1 = coord1 - 0.005 / 10;
      coord2 = coord2 + 0.005;

      map.panTo([coord1, coord2], {
        duration: duration,
        delay: 0,
        timingFunction: 'linear',
        flying: false
      }).then(function () {
        animatedMap();
      });

    };

    animatedMap();

  }

}
