function fullPageInit() {
  $('#fullpage').fullpage({
    menu: '#nav',
    anchors: ['slide1', 'slide2', 'slide3', 'slide4', 'slide5', 'slide6', 'slide7', 'slide8', 'slide9', 'slide10', 'slide11'],
    css3: true,
    scrollingSpeed: 1000,
    fitToSectionDelay: 0,
    licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE',
    onLeave: function(origin, destination, direction){
        var targetSection = destination.item;

        if(direction =='up'){
          $(targetSection).find('.feature').css('transition', 'none');
        }      

        if(direction =='down'){
          $(targetSection).find('.feature').css('transition', 'opacity .7s 1s');
        }
    }
  });
}

$(function () {

  if ($(window).width() >= 992) {
    fullPageInit();
  }

});

$(window).on('resize', function () {

  if ( ($(window).width() >= 992) && (!$('html').hasClass('fp-enabled')) ) {
    fullPageInit();
    console.log('init');
  } else if ( ($(window).width() < 992) && ($('html').hasClass('fp-enabled')) ) {
    fullpage_api.destroy('all');
    console.log('destroy');
  }

});


$(window).on('resize', function () {
  location.reload();
});