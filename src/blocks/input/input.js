function labelFly() {
  var inputWrapper = document.querySelectorAll('.js-label-fly');

	function listenChange(inputEl, inputWrapper) {
		if (inputEl.value !== '') {
			if (!inputWrapper.classList.contains('active')) {
				inputWrapper.classList.add('active')
			}
		} else {
			if (inputWrapper.classList.contains('active')) {
				inputWrapper.classList.remove('active')
			}
		}
	}

	Array.prototype.forEach.call(inputWrapper, function(item) {
    var inputEl = item.querySelector('.input');

    if (inputEl.value !== '') {
      item.classList.add('active');
    }
    inputEl.addEventListener('change', listenChange.bind(null, inputEl, item));
  });
}

labelFly();